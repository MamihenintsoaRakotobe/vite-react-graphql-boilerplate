import { configureStore } from "@reduxjs/toolkit";
import {useDispatch} from 'react-redux';
import SnackBarReducer from "../ducks/Snackbar.slice";
import UserReducer from "../ducks/user.slice";




const store = configureStore({
  reducer: {
    user: UserReducer,
    snack: SnackBarReducer,
  
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch; // Export a hook that can be reused to resolve types

export default store;
