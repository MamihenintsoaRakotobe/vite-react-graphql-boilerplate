import { loginTranslate } from "@translate/fr/login";
import { roleTranslate } from "@translate/fr/role";
import { userTranslate } from "@translate/fr/user";
import i18n from "i18next";
import { commonTranslate } from "@translate/fr/common";
import { initReactI18next } from "react-i18next";

const resources = {
  fr: {
    translation: {
      common: commonTranslate,
      login: loginTranslate,
      user: userTranslate,
      role: roleTranslate,
    },
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "fr", // language to use, more information here: https://www.i18next.com/overview/configuration-options#languages-namespaces-resources
    // you can use the i18n.changeLanguage function to change the language manually: https://www.i18next.com/overview/api#changelanguage
    // if you're using a language detector, do not define the lng option

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
