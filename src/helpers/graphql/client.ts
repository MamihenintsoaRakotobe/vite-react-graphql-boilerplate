import {ApolloClient, ApolloLink, fromPromise, HttpLink, InMemoryCache, split} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { onError } from "@apollo/client/link/error";
import {WebSocketLink} from '@apollo/client/link/ws';
import {getMainDefinition} from '@apollo/client/utilities';
import { setUserLoggedToken } from "@ducks/user.slice";
import store from "../../store/store";

const { VITE_SERVER_URL, VITE_SERVER_WS_URL, VITE_SERVER_TOKEN } = import.meta.env;

const httpLink: HttpLink = new HttpLink({
  uri: VITE_SERVER_URL,
});

const wsLink = new WebSocketLink({
  uri: VITE_SERVER_WS_URL,
  options: {
    reconnect: true,
  },
});

const authLink: ApolloLink = setContext(async (_, { headers }) => {
  const token = localStorage.getItem("@accessToken");
  return {
    headers: {
      ...headers,
      authorization: token ?? VITE_SERVER_TOKEN,
    },
  };
});

const getNewToken = async () => {
  const refreshToken = localStorage.getItem("@refreshToken");
  const response = await fetch(VITE_SERVER_URL, {
    headers: { authorization: VITE_SERVER_TOKEN, "content-type": "application/json" },
    method: "POST",
    body: JSON.stringify({
      query: `mutation refreshToken($refreshToken: String!) {
        refreshToken(data: {
          refreshToken: $refreshToken
        }) {
          access_token
        }
      }
    `,
      variables: {
        refreshToken,
      },
    }),
  });

  const res = await response.json();

  if (res.data) {
    localStorage.setItem("@accessToken", res.data.refreshToken.access_token);
    return res.data.refreshToken.access_token;
  }

  localStorage.removeItem("@refreshToken");
  localStorage.removeItem("@accessToken");

  store.dispatch(setUserLoggedToken({}));
};

const errorLink = onError(({ graphQLErrors, operation, forward }) => {
  if (graphQLErrors) {
    if (graphQLErrors[0].extensions.describe === "JWT_EXPIRED") {
      return fromPromise(
        getNewToken().catch(() => {
          return;
        })
      )
        .filter((value) => Boolean(value))
        .flatMap((accessToken) => {
          const oldHeaders = operation.getContext().headers;
          operation.setContext({
            headers: {
              ...oldHeaders,
              authorization: accessToken,
            },
          });
          return forward(operation);
        });
    }
  }
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

const clientGraphQL = new ApolloClient({
  link: ApolloLink.from([authLink, errorLink, splitLink]),
  cache: new InMemoryCache(),
});

export default clientGraphQL;
