const parseError = (error: any) => JSON.parse(JSON.stringify(error));

export const isNotUnique = (error: any): boolean => {
  if (parseError(error).message === "NOT_UNIQ") {
    return true;
  }

  return false;
};
