export interface BaseEntity {
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date
}

export interface FormSelectItem {
  value: any
  content: string
  warningMessage?: string
  dependencyId?: number | null
}

export interface FormAutocompleteItem {
  value: any
  label: string
}

export interface BasicObject {
  [key: string]: any
}

export interface SelectMultipleArrayContent {
  value: number | string
  content: string
}

export interface GroupedCheckboxArrayContent {
  name: string
  label: string
}
