import { ApolloQueryResult } from "@apollo/client";
import { Tooltip } from "@mui/material";
import { GridColumnHeaderParams, GridRenderCellParams } from "@mui/x-data-grid";
import type { Location } from "@remix-run/router";
import styles from "@styles/Common.module.scss";

export const renderCellWithTooltip = (params: GridRenderCellParams) => (
  <Tooltip title={params.row[params.field] ?? ""} enterDelay={800}>
    <span className={styles.tableCellTruncate}>{params.row[params.field] ?? ""}</span>
  </Tooltip>
);

export const renderHeaderWithTooltip = (params: GridColumnHeaderParams) => <strong>{params.colDef.headerName}</strong>;

export const refreshList = async (
  l: Location,
  refetch: (variables?: Partial<any> | undefined) => Promise<ApolloQueryResult<any>>
) => {
  if (l.state === "reload") {
    l.state = "";
    await refetch();
  }
};

export const filterListReturnSelect = (data: any[]) =>
  data.filter((d) => {
    if (d !== "") {
      return {
        value: d,
        content: d,
      };
    }
  });
