import { Tooltip } from '@mui/material';
import { GridRenderCellParams } from '@mui/x-data-grid';
import styles from '@styles/Common.module.scss';
import { MutableRefObject } from 'react';
import { ObjectSchema } from 'yup';

export const renderCellWithTooltip = (params: GridRenderCellParams) => (
  <Tooltip title={params.row[params.field] ?? ''} enterDelay={800}>
    <span className={styles.tableCellTruncate}>
      {params.row[params.field] ?? ''}
    </span>
  </Tooltip>
);

export const unselect = (ref: MutableRefObject<null>) => {
  if (ref.current) {
    const current: Document = ref.current;
    const row = Array.from(current.getElementsByClassName('MuiDataGrid-row'));
    setTimeout(() => {
      const selected = row.filter((f) => f.ariaSelected === 'true')[0];
      const evt = new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window,
        detail: 0,
        screenX: 0,
        screenY: 0,
        clientX: 0,
        clientY: 0,
        ctrlKey: true,
        altKey: false,
        shiftKey: false,
        metaKey: false,
        button: 0,
        relatedTarget: null,
      });
      selected.dispatchEvent(evt);
    }, 100);
  }
};

export const validateImageFormat = (file: File): boolean =>
  file &&
  ['image/jpeg', 'image/png'].includes(file.type) &&
  Math.round(file.size / 1024) < 1024;

export const validateFileFormat = (file: File): boolean => {
  return (
    file &&
    ['application/pdf'].includes(file.type) &&
    Math.round(file.size / (5 * 1024)) < 5 * 1024
  );
};

export const isRequired = ({
  field,
  validationSchema,
}: {
  field: string
  validationSchema: ObjectSchema<any>
}) => {
  return validationSchema.fields[field]?.exclusiveTests?.required || false;
};

export const formatDate = (date: string) => {

  const d = new Date(date);
  let month = '' + (d.getMonth() + 1);
  let day = '' + d.getDate();
  const year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
};
export const downloadBlob = (file: File, name = 'file.txt') => {
  const fr = new FileReader();
  fr.readAsArrayBuffer(file);

  fr.onload = () => {
    if (fr.result) {
      const blob = new Blob([fr.result]);

      // Convert your blob into a Blob URL (a special url that points to an object in the browser's memory)
      const blobUrl = URL.createObjectURL(blob);

      // Create a link element
      const link = document.createElement('a');

      // Set link's href to point to the Blob URL
      link.href = blobUrl;
      link.download = name;

      // Append link to the body
      document.body.appendChild(link);

      // Dispatch click event on the link
      // This is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(
        new MouseEvent('click', {
          bubbles: true,
          cancelable: true,
          view: window,
        }),
      );

      // Remove link from body
      document.body.removeChild(link);
    }
  };
};


export const hexToRgbA = (hex:string) =>{
  let c:any;
  if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
    c= hex.substring(1).split('');
    if(c.length== 3){
      c= [c[0], c[0], c[1], c[1], c[2], c[2]];
    }
    c= '0x'+c.join('');
    return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',0.2)';
  }
  throw new Error('Bad Hex');
};