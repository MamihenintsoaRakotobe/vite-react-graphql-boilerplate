export const userTranslate = {
  form: {
    button: {
      createUser: "Nouveau utilisateur",
      updateUser: "Modifier utilisateur",
      deleteUser: "Supprimer utilisateur",
    },
    error: {
      atLeastOneRole: "Au moins un role est requis",
      specificEmail: "Tous les emails doivent avoir comme domaine eleofrance.com",
    },
  },
  dialog: {
    delete: {
      user: {
        title: "Suppression d'un utilisateur",
        confirmMessage: "Etes-vous sur de vouloir supprimer cet utilisateur ?",
        userToDelete: "Utilisateur à supprimer :",
      },
    },
  },
  success: {
    login: "Vous êtes connecté !",
    create: "Utilisateur créé avec succès",
  },
};
