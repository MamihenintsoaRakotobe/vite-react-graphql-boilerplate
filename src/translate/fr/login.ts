export const loginTranslate = {
  title: "Connexion",
  titleTab: "Titre Tab",
  labelEmail: "Email",
  loginButton: "Se connecter",
  badLoginError: "Error during log in",
  labelPassword: "Mot de passe",
  error: {
    badCredential: "Mauvais login/Mot de passe",
  },
  success: {
    login: "Vous êtes connecté",
  },
};
