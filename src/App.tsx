import { routeConnected } from "@config/navigation";
import { SeverityEnum } from "@ducks/Severity.enum";
import { closeSnackBar } from "@ducks/Snackbar.slice";
import { setUserLoggedToken } from "@ducks/user.slice";
import {Alert, createTheme, Snackbar, ThemeProvider} from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Navigate, Route, Routes } from "react-router-dom";
import "./App.css";
import "./i18n";
import { HomeScreen } from "./components/screen/home";

type Store = {
  snack: {
    opened: boolean;
    message: string;
    severity: SeverityEnum;
  };
};

const theme = createTheme({
  palette: {
    primary: {
      main: '#0067AC',
    },
    secondary: {
      main: '#ffffff',
    },
  },
});

const App = () => {
  const dispatch = useDispatch();

  
  const snackBarOpenedState = useSelector((state: Store): boolean => state.snack.opened);
  const snackBarMessage = useSelector((state: Store): string => state.snack.message);
  const snackBarSeverity = useSelector((state: Store): SeverityEnum => state.snack.severity);

  

  useSelector((state: any) => state.user.refresh_token);

  const initComponent = () => {
    if (localStorage.getItem("@refreshToken")) {
      dispatch(
        setUserLoggedToken({
          access_token: localStorage.getItem("@accessToken"),
          refresh_token: localStorage.getItem("@refreshToken"),
        })
      );
    }
  };

  useEffect(() => {
    initComponent();
  }, []);

  return (
    <Router>

      <ThemeProvider theme={theme}>

        <Snackbar open={snackBarOpenedState} autoHideDuration={2000} onClose={() => dispatch(closeSnackBar())}>

          <Alert severity={snackBarSeverity} color={snackBarSeverity}>
            {snackBarMessage}
          </Alert>
        </Snackbar>

        {localStorage.getItem("@refreshToken") ? (
          <Routes>
            {routeConnected.map((rc, i) => (
              <Route key={i} path={rc.path} element={rc.element} />
            ))}
          </Routes>
        ) : (
          <Routes>
            <Route path="*" element={<Navigate to="/login" />} />
            <Route path="/login" element={<HomeScreen />} />
          </Routes>
        )}

      </ThemeProvider>

    </Router>
  );
};

export default App;
