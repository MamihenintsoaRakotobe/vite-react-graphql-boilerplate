import { createSelector, createSlice } from "@reduxjs/toolkit";
import jwt_decode from 'jwt-decode';

type DecodedToken = {
  username: string;
  id: number;
  role: string[];
  firstName: string;
  lastName: string;
}

export const userSlice = createSlice({
  name: "user",
  initialState: {
    access_token: null,
    refresh_token: null,
    firstName: "",
    lastName: "",
    id: 0,
    roles: [],
    email: "",
  },
  reducers: {
    setUserLoggedToken: (state, action) => {

      const decodedToken: DecodedToken = jwt_decode(action.payload.access_token);

      state.access_token = action.payload.access_token;
      state.refresh_token = action.payload.refresh_token;
      state.firstName = decodedToken.firstName;
      state.lastName = decodedToken.lastName;
      state.id = decodedToken.id;
      state.email = decodedToken.username;
      state.roles = decodedToken.role as [];
    },
    updateUserFirstName: (state, action) => {
      state.firstName = action.payload;
    },
    updateUserLastName: (state, action) => {
      state.lastName = action.payload;
    },
    updateUserRoles: (state, action) => {
      state.roles = action.payload;
    },
    updateUserEmail: (state, action) => {
      state.email = action.payload;
    },
    updateUserId: (state, action) => {
      state.id = action.payload;
    },
  },
});

const getSlice = (state: any) => state.user;

// Action creators are generated for each case reducer function
export const { setUserLoggedToken, updateUserFirstName, updateUserLastName, updateUserId } = userSlice.actions;

export const getUserLoggedToken = createSelector([getSlice], (state) => ({
  access_token: state.access_token,
  refresh_token: state.refresh_token,
}));
export const getUserFirstName = createSelector([getSlice], (state) => state.firstName);
export const getUserLastName = createSelector([getSlice], (state) => state.lastName);
export const getUserId = createSelector([getSlice], (state) => state.id);
export const isUserLoggedIn = createSelector([getSlice], (state) => state.access_token !== null && state.refresh_token !== null);
export const getUser = createSelector([getSlice], (state) => ({
  firstName: state.firstName,
  lastName: state.lastName,
  id: state.id,
  roles: state.roles,
  email: state.email,
}));

export default userSlice.reducer;
