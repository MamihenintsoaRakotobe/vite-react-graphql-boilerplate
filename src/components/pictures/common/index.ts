export { default as EyeOpen } from "./EyeOpen";
export { default as EyeSlash } from "./EyeSlash";
export { default as FileXls } from "./FileXls";
export { default as Pen } from "./Pen";
export { default as Trash } from "./Trash";

