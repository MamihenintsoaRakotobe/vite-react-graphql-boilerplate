import { FunctionComponent } from "react";
import { GenericGoBack } from ".";
import {useNavigate} from 'react-router-dom';

const GoBack: FunctionComponent = () => {
  const navigate = useNavigate();

  const goBack = () => {
    navigate(-1);
  };

  return <GenericGoBack goBack={goBack} />;
};

export default GoBack;
