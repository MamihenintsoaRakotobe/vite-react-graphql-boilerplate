import {
  Box,
  Tab,
  Tabs as TabComponent,
} from "@mui/material";
import styles from "@styles/Admin.module.scss";
import { TabsContent } from "@type/global/common";
import React, {
  FunctionComponent,
  ReactNode,
} from "react";



interface Props {
  content: TabsContent[];
  rightComponent?: ReactNode;
  firstComponent?: ReactNode;
}

const Tabs: FunctionComponent<Props> = ({ content, rightComponent, firstComponent }) => {
  const [value, setValue] = React.useState(0);
  

  const handleChange = (_: React.SyntheticEvent, v: number) => {
    
    setValue(v);
    
  };

  return (

    <Box>
      <div className={styles.containerTabsWithRightC}>
        <Box className={styles.adminTabsContainer}>
          <TabComponent
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="auto"
            TabIndicatorProps={{ style: { background: "transparent" } }}
            sx={{
              "& button.Mui-selected": { backgroundColor: "#0067AC", color: "#ffffffff", borderRadius: "6px" },
            }}
          >
            {content.map((c) => (
              <Tab key={c.id} className={styles.adminTabsTitle} label={c.label}/>
            ))}
          </TabComponent>
        </Box>
        {rightComponent}
      </div>
      {firstComponent && <div className={styles.containerFirstComponentTabsG}>{firstComponent}</div>}
      {content.find((a) => a.id === value)?.content}
     
    </Box>

  );
};

export default Tabs;
