import { GridRenderCellParams, useGridApiContext } from '@mui/x-data-grid';
import { Dispatch, FC, SetStateAction, useEffect } from 'react';
import { GenericDatePicker } from '@components/common/form';
import styles from './DatePickerEditableCell.module.scss';
import dayjs from 'dayjs';
import { useForm } from 'react-hook-form';

interface IDatePickerEditableCellProps {
  props: GridRenderCellParams<any, any, any>
  name: string
  
  setCellFormData:Dispatch<SetStateAction<any>>
}

const DatePickerEditableCell: FC<IDatePickerEditableCellProps> = ({
  props,
  name,
  setCellFormData,
}) => {
  const { id, field }  = props satisfies GridRenderCellParams<any, any, any>;

  const apiRef = useGridApiContext();
  const {
    control,
    formState: { errors },
    watch,  
  } = useForm();

  const saveCellChange = async () => {
    const value = dayjs(watch(name)).format('DD/MM/YYYY');
    apiRef.current.setEditCellValue({ id, field, value });
   
    const data:any = {
      rowId:Number(id),
    };
    data[name] =watch(name); 
    setCellFormData(data);
  };

  useEffect(() => {
    saveCellChange();
  }, [watch(name)]);

  return (
    <div className={styles.selectContainer}>
      <GenericDatePicker
        control={control}
        errors={errors}
        name={name}
        label={''}
      />
    </div>
  );
};

export default DatePickerEditableCell;
