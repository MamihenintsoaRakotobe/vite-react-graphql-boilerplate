import { GridRenderCellParams, useGridApiContext } from '@mui/x-data-grid';
import { CSSProperties, Dispatch, FC, SetStateAction } from 'react';
import {   useForm } from 'react-hook-form';

import { Input } from '@components/common/form';
import styles from './InputEditableCell.module.scss';

interface IInputEditableCellProps {
  props: GridRenderCellParams
  name: string
  setCellFormData:Dispatch<SetStateAction<any>>
  style?:CSSProperties
}

const InputEditableCell: FC<IInputEditableCellProps> = ({
  props,
  name,
  setCellFormData,
  style,
}) => {
  const { id,  field } = props satisfies GridRenderCellParams<any, any, any>;

  const apiRef = useGridApiContext();
  const {
    control,
    formState: { errors },
    
    getValues,  
  } = useForm();
  
 

  const saveCellChange = async () => {
    apiRef.current.setEditCellValue({ id, field, value: getValues(name) });
    const data:any = {
      rowId:Number(id),
    };
    data[name] =getValues(name); 
    setCellFormData(data);
  };

  const onBlur= () => {
    saveCellChange();
  };

  const onKeyDown = (e: any) => {
    if(e.code ==='Enter'){
      saveCellChange();
    }
  };

  
  return (
    <div className={styles.selectContainer} style={style}>
      <Input  control={control} errors={errors} name={name} onBlur={onBlur} onKeyDownFunc={(e)=>onKeyDown(e)}/>
    </div>
  );
};

export default InputEditableCell;
