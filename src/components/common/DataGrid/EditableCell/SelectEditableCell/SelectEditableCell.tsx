import { GridRenderCellParams, useGridApiContext } from '@mui/x-data-grid';
import { Dispatch, FC, SetStateAction, useEffect } from 'react';
import {  FormSelectItem } from '@helpers/models/common';
import { Select } from '@components/common/form';
import styles from './SelectEditableCell.module.scss';
import { useForm } from 'react-hook-form';

interface ISelectEditableCellProps {
  props: GridRenderCellParams
  items: FormSelectItem[]
  name: string
  setCellFormData:Dispatch<SetStateAction<any>>
}

const SelectEditableCell: FC<ISelectEditableCellProps> = ({
  props,
  items,
  name,
  setCellFormData,
}) => {
  const { id, field } = props satisfies GridRenderCellParams<any, any, any>;

  const apiRef = useGridApiContext();
  const {
    control,
    formState: { errors },
    watch,  
  } = useForm();

  const saveCellChange = async () => {
    const value =
      typeof watch(name) === 'number'
        ? items.find((i) => i.value === watch(name))?.content
        : watch(name);
    apiRef.current.setEditCellValue({ id, field, value });
    const data:any = {
      rowId:Number(id),
    };
    data[name] =watch(name);
    setCellFormData(data); 
  };

  useEffect(() => {
    saveCellChange();
  }, [watch(name)]);

  return (
    <div className={styles.selectContainer}>
      <Select control={control} errors={errors} name={name} items={items} containerStyle={{width:'100%'}}/>
    </div>
  );
};

export default SelectEditableCell;
