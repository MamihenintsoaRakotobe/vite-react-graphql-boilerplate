import { Backdrop, CircularProgress } from "@mui/material";
import React, { FunctionComponent } from "react";

interface Props {
  loading: boolean;
}

const LoadingOverlay: FunctionComponent<Props> = ({ loading = false }) => {
  return (
    <Backdrop sx={{ zIndex: (theme) => theme.zIndex.drawer + 99999999 }} open={loading}>
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};

export default LoadingOverlay;
