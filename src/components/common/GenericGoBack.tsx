import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { IconButton } from "@mui/material";
import { FunctionComponent } from "react";

interface Props {
  goBack: () => void
}

const GenericGoBack: FunctionComponent<Props> = ({goBack}) => {
  return (
    <IconButton onClick={goBack}>
      <ArrowBackIcon />
    </IconButton>
  );
};

export default GenericGoBack;
