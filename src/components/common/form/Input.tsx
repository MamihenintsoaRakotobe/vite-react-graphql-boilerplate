import { BasicObject } from "@helpers/models/common";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import HelpIcon from "@mui/icons-material/Help";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import { OutlinedInputProps, TextField, Tooltip } from "@mui/material";
import styles from "@styles/Common.module.scss";
import { CSSProperties, FocusEventHandler, FunctionComponent, HTMLInputTypeAttribute, KeyboardEventHandler } from "react";
import { Control, Controller } from "react-hook-form";

interface Props {
  control: Control<any, BasicObject>;
  name: string;
  defaultValue?: unknown;
  label?: string;
  placeholder?: string;
  errors: BasicObject;
  required?: boolean;
  type?: HTMLInputTypeAttribute;
  multiline?: boolean;
  minRows?: number;
  maxRows?: number;
  disabled?: boolean;
  rules?: { [key: string]: any };
  customErrorName?: string;
  InputProps?: Partial<OutlinedInputProps>;
  rows?: number;
  inlineLabel?: boolean;
  containerStyle?: CSSProperties;
  tooltip?: string | null;
  isValidCode?: "valid" | "notValid" | "null";
  onBlur?: FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  style?: CSSProperties;
  className?: string;
  displayErrorMessage?: boolean;
  ref?:any
  //onKeyDown?: (param:KeyboardEvent<HTMLDivElement>) => void
  onKeyDownFunc?: KeyboardEventHandler<HTMLDivElement> 
  inputWrapperClassName?:string
}

const Input: FunctionComponent<Props> = ({
  control,
  name,
  defaultValue = "",
  label = "",
  errors,
  required = false,
  placeholder = "",
  type = "text",
  multiline = false,
  minRows = 0,
  maxRows = 0,
  disabled = false,
  rules = {},
  customErrorName,
  InputProps,
  rows,
  inlineLabel = false,
  containerStyle,
  tooltip = null,
  isValidCode = "null",
  onBlur = undefined,
  className = "",
  displayErrorMessage = true,
  ref = null,
  onKeyDownFunc,
  inputWrapperClassName,
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field }) => (
        <div className={inlineLabel ? styles.inLineLabelContainer : ""} style={containerStyle ?? {}}>
          <label htmlFor={name} className={inlineLabel ? styles.labelInputInline : styles.labelInput}>
            <span style={inlineLabel  ? {marginRight:"1rem"}:{}}>
              {label}
              {required && label?.length ? <span className={styles.requiredStar}>*</span> : ""}
            </span>

            {isValidCode !== "null" &&
              (isValidCode === "valid" ? (
                <CheckCircleOutlineIcon color={"success"} sx={{ marginLeft: "10px" }} />
              ) : (
                <HighlightOffIcon color={"error"} sx={{ marginLeft: "10px" }} />
              ))}

            {tooltip && (
              <Tooltip title={tooltip} sx={{ marginLeft: "10px" }}>
                <HelpIcon />
              </Tooltip>
            )}
          </label>
          <div className={inputWrapperClassName ?? styles.inputContainer}>
            <TextField
              {...field}
              //required={required}
              className={className}
              id={name}
              variant="outlined"
              placeholder={placeholder}
              FormHelperTextProps={{ style: { backgroundColor: "transparent", margin: 0, padding: "5px" } }}
              helperText={
                displayErrorMessage &&
                !!errors &&
                errors[customErrorName ?? name] &&
                errors[customErrorName ?? name].message
              }
              type={type}
              error={!!errors && errors[customErrorName ?? name]}
              multiline={multiline}
              minRows={minRows}
              maxRows={maxRows ? maxRows : undefined}
              disabled={disabled}
              size="small"
              InputProps={InputProps}
              rows={rows}
              onBlur={onBlur}
              inputRef={ref}
              onKeyDown={onKeyDownFunc}
            />
          </div>
        </div>
      )}
    />
  );
};

export default Input;
