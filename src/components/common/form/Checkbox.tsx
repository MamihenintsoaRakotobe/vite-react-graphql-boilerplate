import { BasicObject } from "@helpers/models/common";
import { Checkbox as CheckboxComponent, FormControlLabel } from "@mui/material";
import { ChangeEvent, FunctionComponent } from "react";
import { Control, Controller, UseFormGetValues } from "react-hook-form";

interface Props {
  control: Control<any, BasicObject>;
  name: string;
  defaultValue?: unknown;
  label?: string | JSX.Element;
  disabled?: boolean;
  getValues: UseFormGetValues<any>;
  handleChange: ((event: ChangeEvent<HTMLInputElement>, checked: boolean) => void) | undefined;
  value?: string;
  isMultiple?: boolean;
}

const Checkbox: FunctionComponent<Props> = ({
  control,
  name,
  defaultValue = false,
  label = "",
  disabled = false,
  handleChange,
  getValues,
  value,
  isMultiple = false,
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      render={({ field }) => (
        <FormControlLabel
          control={
            <CheckboxComponent
              disabled={disabled}
              checked={isMultiple ? (getValues(name) ?? []).indexOf(value) > -1 : getValues(name)}
              {...field}
              onChange={handleChange}
            />
          }
          label={label}
        />
      )}
    />
  );
};

export default Checkbox;
