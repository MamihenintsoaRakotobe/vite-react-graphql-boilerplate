import { useEffect, useState } from 'react';
import { Control, Controller } from 'react-hook-form';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { Dayjs } from 'dayjs';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import TextField from '@mui/material/TextField';
import styles from './GenericDatePicker.module.scss';

type BasicDatePickerProps = {
  label: string
  name: string
  control: Control<any, any>
  errors: any
  className?: string
  textFieldClassName?: string
  inlineLabel?: boolean
  defaultValue?: Dayjs | null
}

const GenericDatePicker = ({
  label,
  name,
  control,
  errors,
  className,
  textFieldClassName,
  inlineLabel = false,
  defaultValue = null,
}: BasicDatePickerProps) => {
  const [value, setValue] = useState<Dayjs | null>(defaultValue);

  useEffect(() => {
    setValue(defaultValue);
  }, [defaultValue]);

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange } }) => {
        return (
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DatePicker
              value={value}
              inputFormat="DD/MM/YYYY"
              onChange={(newValue) => {
                onChange(newValue);
                setValue(newValue);
              }}
              InputProps={{
                className: `${styles.date}`.concat(className ?? ''),
              }}
              renderInput={(params) => (
                <div className={inlineLabel ? styles.inlineLabel : ''}>
                  <div className={inlineLabel ? styles.label : ''}>{label}</div>
                  <TextField
                    {...params}
                    id={name}
                    className={textFieldClassName}
                    error={!!errors && errors[name]}
                    helperText={
                      'error custom message' &&
                      !!errors &&
                      errors[name] &&
                      errors[name].message
                    }
                    sx={{
                      '.MuiInputBase-input': {
                        padding: 0,
                        background: '#ffff',
                      },
                    }}
                  />
                </div>
              )}
            />
          </LocalizationProvider>
        );
      }}
    />
  );
};

export default GenericDatePicker;
