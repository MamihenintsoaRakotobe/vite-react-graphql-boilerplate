import { Add } from '@mui/icons-material';
import React from 'react';
import { Link, LinkProps } from 'react-router-dom';

interface IProps {
  props: LinkProps
  children?: JSX.Element
}

const ButtonExternLink: React.FC<IProps> = ({ props, children }) => {
  return <Link {...props}>{children ?? <Add />}</Link>;
};

export default ButtonExternLink;
