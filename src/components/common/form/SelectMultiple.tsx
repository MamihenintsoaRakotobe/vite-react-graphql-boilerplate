import { BasicObject } from "@helpers/models/common";
import {
  Checkbox,
  FormControl,
  FormHelperText,
  ListItemText,
  MenuItem,
  Select as SelectComponent,
} from "@mui/material";
import styles from "@styles/Common.module.scss";
import { CSSProperties, FunctionComponent, useEffect } from "react";
import { FieldValues, UseFormGetValues, UseFormSetValue } from "react-hook-form";

interface Props {
  arrayContent: { value: string | number; content: string }[];
  defaultValue?: number[];
  name: string;
  label?: string;
  error: BasicObject;
  required?: boolean;
  setValue: UseFormSetValue<FieldValues>;
  getValues: UseFormGetValues<FieldValues>;
  variant?: "standard" | "filled" | "outlined";
  style?: BasicObject;
  disabled?: boolean;
  nbDisplayedItemInList?: number;
  inlineLabel?:boolean;
  containerStyle?:CSSProperties
  className?:string
  
}

const SelectMultiple: FunctionComponent<Props> = ({
  arrayContent,
  defaultValue,
  name,
  label = "",
  error,
  required = false,
  setValue,
  getValues,
  variant = "outlined",
  style,
  disabled = false,
  nbDisplayedItemInList = 4,
  inlineLabel = false,
  containerStyle,
  className,
}) => {
  const ITEM_HEIGHT = 54;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * nbDisplayedItemInList + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  const changeValue = (event: { target: { value: any } }) => {
    
    setValue(name, event.target.value, { shouldValidate: true });
  };

  useEffect(() => {
    setValue(name, []);
  }, [name, setValue]);

  return (
    <FormControl fullWidth>
      <div className={inlineLabel ? styles.inLineLabelContainer :""} style={containerStyle ?? {}}>
        <label htmlFor={name} className={inlineLabel ?styles.labelInputInline : styles.labelInput}>
          <span>{label} {required && (<span className={styles.colorRequiredSpan}>*</span>)}</span>
        </label>
        <div className={styles.inputContainer}>
          <SelectComponent
            multiple
            value={getValues(name) ?? defaultValue ?? []}
            onChange={changeValue}
            variant={variant}
            style={style}
            error={!!error[name]}
            required={required}
            disabled={disabled}
            size="small"
            renderValue={(selected) => {
              return selected
                .map((s: string) => {
                  const content: any = arrayContent.find((aa) => aa.value === s);

                  return content?.content ?? "";
                })
                .join(", ");
            }}
            MenuProps={MenuProps}
            className={className ?? styles.inputStyle}
          >
            {arrayContent.map((content: any, index) => [
              <MenuItem key={index} value={content.value}>
                <Checkbox checked={(getValues(name) ?? []).indexOf(content.value) > -1} />
                <ListItemText primary={content.content} />
              </MenuItem>,
            ])}
          </SelectComponent>
        </div>
       
      </div>
      
      {!!error[name] && <FormHelperText style={{ color: "#d32f2f" }}>{error[name].message}</FormHelperText>}
    </FormControl>
  );
};

export default SelectMultiple;
