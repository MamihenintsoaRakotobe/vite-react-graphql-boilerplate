import { BasicObject, GroupedCheckboxArrayContent } from "@helpers/models/common";
import { Box } from "@mui/material";
import styles from "@styles/Admin.module.scss";
import { FC } from "react";
import { Control, UseFormGetValues, UseFormSetValue } from "react-hook-form";
import Checkbox from "./Checkbox";

interface IProps {
  groupeTitle: string;
  control: Control<any, BasicObject>;
  info: GroupedCheckboxArrayContent[];
  getValues: UseFormGetValues<any>;
  setValue: UseFormSetValue<any>;
  name: string;
}

const GroupedCheckbox: FC<IProps> = ({ groupeTitle, control, info, getValues, name, setValue }) => {
  const handleChange = (value: string) => {
    const array = getValues(name) ?? [];
    const index = array.indexOf(value);
    const arrayTmp = [...array];
    if (index > -1) {
      array.splice(index, 1);
      setValue(name, array);
    } else {
      // add value
      arrayTmp.push(value);
      setValue(name, arrayTmp);
    }
  };

  return (
    <Box>
      <h4>{groupeTitle}</h4>
      <Box className={styles.checkboxContainer}>
        {info.map((i) => (
          <Checkbox
            key={i.name}
            name={name}
            control={control}
            handleChange={() => handleChange(i.name)}
            label={i.label}
            value={i.name}
            getValues={getValues}
            isMultiple={true}
          />
        ))}
      </Box>
    </Box>
  );
};

export default GroupedCheckbox;
