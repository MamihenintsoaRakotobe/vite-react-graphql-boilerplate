import { BasicObject, FormSelectItem } from '@helpers/models/common';
import HelpIcon from '@mui/icons-material/Help';
import {
  FormHelperText,
  MenuItem,
  Select as SelectComponent,
  Tooltip,
} from '@mui/material';
import styles from '@styles/Common.module.scss';
import { CSSProperties, FunctionComponent } from 'react';
import { Control, Controller } from 'react-hook-form';

interface Props {
  control: Control<any, BasicObject>
  name: string
  defaultValue?: number | string
  label?: string
  items: FormSelectItem[]
  required?: boolean
  labelRight?: any
  errors?: BasicObject
  customErrorName?: string
  disabled?: boolean
  inlineLabel?: boolean
  className?: string
  containerStyle?: CSSProperties
  tooltip?: string | null
}

const Select: FunctionComponent<Props> = ({
  control,
  name,
  defaultValue = '',
  label = '',
  items,
  required = false,
  labelRight,
  errors,
  customErrorName,
  disabled = false,
  inlineLabel = false,
  className,
  containerStyle,
  tooltip = null,
}) => {
  const values = items.map((i) => i.value);
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      render={({ field }) => (
        <div
          className={inlineLabel ? styles.inLineLabelContainer : ''}
          style={containerStyle ?? {}}
        >
          {label.length > 0 && (
            <label
              htmlFor={name}
              className={
                inlineLabel ? styles.labelInputInline : styles.labelInput
              }
              style={{ display: 'flex', justifyContent: 'space-between' }}
            >
              <span>
                {label}
                {required && label?.length ? (
                  <span className={styles.requiredStar}>*</span>
                ) : (
                  ''
                )}
                {labelRight}
              </span>
              {tooltip && (
                <Tooltip title={tooltip} sx={{ marginLeft: '10px' }}>
                  <HelpIcon />
                </Tooltip>
              )}
            </label>
          )}

          <SelectComponent
            {...field}
            id={name}
            name={name}
            className={className ?? styles.inputStyle}
            variant="outlined"
            value={values.includes(field.value) ? field.value : ''}
            defaultValue={
              field.value ?? (items.length && items[0].value) ?? null
            }
            size="small"
            disabled={disabled}
            error={!!errors && !!errors[customErrorName ?? name]}
            style={{ background: '#ffff' }}
          >
            {items.map((item, index) => (
              <MenuItem
                key={index}
                value={item.value}
                style={{ minHeight: '36px' }}
              >
                {item.content}
              </MenuItem>
            ))}
          </SelectComponent>

          {!!errors && !!errors[customErrorName ?? name] && (
            <FormHelperText style={{ color: '#d32f2f', marginLeft: '14px' }}>
              {errors[customErrorName ?? name].message}
            </FormHelperText>
          )}
        </div>
      )}
    />
  );
};

export default Select;
