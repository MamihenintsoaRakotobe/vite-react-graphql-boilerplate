import * as React from 'react';
import TextField from '@mui/material/TextField';
import MuiAutocomplete from '@mui/material/Autocomplete';
import { FC } from 'react';
import styles from './Autocomplete.module.scss';
import { Control, Controller} from 'react-hook-form';
import { BasicObject } from '@helpers/models/common';


export interface FormAutocompleteItem {
    value: any;
    label: string;
  }

interface IProps {
    control: Control<any, BasicObject>;
    name: string;
    item:FormAutocompleteItem []
    label?:string
    inlineLabel?: boolean;
    required?: boolean;
    customErrorName?: string;
    errors: BasicObject;
    displayErrorMessage?: boolean;
    autoCompleteKey?:any
}

const Autocomplete:FC<IProps> = ({
  control,
  name,
  item,
  label = '',
  inlineLabel = false,
  required = false,
  errors,
  customErrorName,
  displayErrorMessage = true,
  autoCompleteKey,
 
}) => {

  
  return (
    <Controller
      name={name}
      key={autoCompleteKey}
      control={control}
      render={({field}) => (
        <div className={styles.container}>
          {label && (
            <label htmlFor={name} className={inlineLabel ? styles.labelInputInline : styles.labelInput}>
              <span style={inlineLabel  ? {marginRight:"1rem"}:{}}>
                {label}
                {required && label?.length ? <span className={styles.requiredStar}>*</span> : ""}
              </span>
            </label>
          )}
          <MuiAutocomplete
            disablePortal
            options={item}
            className={styles.muiInput}
            getOptionLabel={(option) =>
              option?.label ??
                  item.find(({ value }) => value === option)?.label ??
                  ''
            }
            isOptionEqualToValue={(option, value) =>
              option.value === value.value
            }
            onChange={(_event, data) => field.onChange(data?.value ?? null)}
            renderInput={(params) => (
              <TextField
                {...field}
                {...params}
                className={styles.input}
                error={!!errors && errors[customErrorName ?? name]}
                helperText={
                  displayErrorMessage &&
                      !!errors &&
                      errors[customErrorName ?? name] &&
                      errors[customErrorName ?? name].message
                }
                // InputProps={{
                //   endAdornment: (
                //     <InputAdornment position='start'>
                //         <SearchIcon />
                //     </InputAdornment>
                //   )
                // }}
              />
            )}
          />
        </div>
      )}/>



  );
};

export default Autocomplete;
