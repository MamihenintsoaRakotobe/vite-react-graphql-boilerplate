import { Button as ButtonComponent } from "@mui/material";
import styles from "@styles/Common.module.scss";
import { FunctionComponent, ReactNode } from "react";

interface Props {
  children: ReactNode;
  className?: string;
  onClick?: () => void;
  disabled?: boolean;
  isRounded?: boolean;
  size?: "small" | "medium" | "large";
  type?: "button" | "submit" | "reset";
  form?: string;
  color?: "inherit" | "secondary" | "primary" | "success" | "error" | "info" | "warning";
}

const Button: FunctionComponent<Props> = ({
  children,
  className = "",
  onClick,
  disabled = false,
  isRounded = false,
  size = "medium",
  type = "submit",
  color = "primary",
  form,
}) => {
  return (
    <ButtonComponent
      className={`${className} ${isRounded ? styles.commonButtonRoudedG : ""}`}
      color={color}
      type={type}
      variant="contained"
      onClick={onClick}
      disabled={disabled}
      size={size}
      form={form}
    >
      {children}
    </ButtonComponent>
  );
};

export default Button;
