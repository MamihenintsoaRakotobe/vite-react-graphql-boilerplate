import {
  renderCellWithTooltip,
  renderHeaderWithTooltip,
} from '@helpers/front/utils';
import { Box } from '@mui/material';
import {
  DataGrid,
  frFR,
  GridCallbackDetails,
  GridCellModesModel,
  GridCellParams,
  GridColumnHeaderParams,
  GridColumns,
  GridExperimentalFeatures,
  GridFeatureMode,
  GridInputSelectionModel,
  GridRenderCellParams,
  GridRowClassNameParams,
  GridRowIdGetter,
  GridRowParams,
  GridSelectionModel,
  GridSlotsComponent,
} from '@mui/x-data-grid';
import { GridInitialStateCommunity } from '@mui/x-data-grid/models/gridStateCommunity';
import styles from '@styles/Common.module.scss';
import React, { forwardRef } from 'react';
import { Dispatch, SetStateAction, useState } from 'react';

interface Props {
  rows: readonly any[]
  columns: GridColumns
  rowPerPageSize?: number
  rowHeight?: number
  headerHeight?: number
  customStyle?: string
  checkboxSelection?: boolean
  hideFooterPagination?: boolean
  autoHeight?: boolean
  rowsPerPageOptions?: number[]
  setStateItemsPerPage?: Dispatch<SetStateAction<number>>
  setChangePage?: Dispatch<SetStateAction<number>>
  initialState?: GridInitialStateCommunity
  rowCount?: number
  pageNumber?: number
  paginationMode?: GridFeatureMode
  loading?: boolean
  sx?: any
  funcClickCell?: (params: GridCellParams) => void
  disableSelectionOnClick?: boolean
  funcClickRow?: (item: any) => void
  ref?: React.Ref<HTMLDivElement>
  getRowId?: GridRowIdGetter<any>
  cellModesModel?: GridCellModesModel
  handleCellModesModelChange?: any
  getRowClassName?:
    | ((params: GridRowClassNameParams<any>) => string)
    | undefined
  showCellRightBorder?: boolean
  experimentalFeatures?: Partial<GridExperimentalFeatures>
  isRowSelectable?: (params: GridRowParams) => boolean
  onSelectionModelChange?: ((selectionModel: GridSelectionModel, details: GridCallbackDetails<any>) => void)
  selectionModel?: GridInputSelectionModel
  components?: Partial<GridSlotsComponent>
}

const Table = forwardRef(
  (
    {
      rows,
      columns,
      rowPerPageSize = 20,
      rowHeight = 52,
      headerHeight = 56,
      customStyle,
      checkboxSelection = false,
      hideFooterPagination = false,
      autoHeight = false,
      rowsPerPageOptions = [10, 20, 50],
      setStateItemsPerPage,
      initialState = undefined,
      rowCount = undefined,
      setChangePage,
      paginationMode = 'client',
      loading = false,
      sx,
      pageNumber,
      funcClickCell,
      disableSelectionOnClick = true,
      funcClickRow,
      getRowId,
      cellModesModel = {},
      handleCellModesModelChange,
      getRowClassName,
      showCellRightBorder = false,
      experimentalFeatures,
      isRowSelectable,
      onSelectionModelChange,
      selectionModel,
      components,
    }: Props,
    ref: React.Ref<HTMLDivElement>,
  ) => {
    Table.displayName = 'Table';

    const [pageSize, setPageSize] = useState<number>(rowPerPageSize);

    const changePageSize = (newPageSize: number) => {
      setPageSize(newPageSize);
      if (setStateItemsPerPage) {
        setStateItemsPerPage(newPageSize);
      }
    };

    const handleChangePage = (pageActual: number) => {
      if (setChangePage) {
        setChangePage(pageActual);
        pageNumber = pageActual;
      }
    };

    const getBoxClassName = () => {
      if (customStyle) {
        return `${customStyle} ${styles.containerTableG}`;
      }
      return styles.containerTableG;
    };

    return (
      <Box className={getBoxClassName()}>
        <DataGrid
          ref={ref}
          
          rowHeight={rowHeight}
          headerHeight={headerHeight}
          rows={rows}
          initialState={initialState}
          columns={[
            ...columns.map((c) => {
              if (!c.renderCell) {
                c.renderCell = (params: GridRenderCellParams) =>
                  renderCellWithTooltip(params);
              }
              if (!c.renderHeader) {
                c.renderHeader = (params: GridColumnHeaderParams) =>
                  renderHeaderWithTooltip(params);
              }

              return c;
            }),
          ]}
          pageSize={pageSize}
          onCellClick={funcClickCell ? (p) => funcClickCell(p) : undefined}
          onPageSizeChange={changePageSize}
          disableSelectionOnClick={disableSelectionOnClick}
          rowsPerPageOptions={rowsPerPageOptions}
          checkboxSelection={checkboxSelection}
          hideFooterPagination={hideFooterPagination}
          autoHeight={autoHeight}
          localeText={frFR.components.MuiDataGrid.defaultProps.localeText}
          rowCount={rowCount}
          onPageChange={handleChangePage}
          page={pageNumber}
          paginationMode={paginationMode}
          loading={loading}
          sx={sx}
          onRowClick={funcClickRow ? (item) => funcClickRow(item) : undefined}
          getRowId={getRowId}
          cellModesModel={cellModesModel}
          onCellModesModelChange={handleCellModesModelChange}
          getRowClassName={getRowClassName}
          showCellRightBorder={showCellRightBorder}
          experimentalFeatures={experimentalFeatures}
          isRowSelectable={isRowSelectable}
          onSelectionModelChange={onSelectionModelChange}
          selectionModel={selectionModel}
          components={components}
          
        />
      </Box>
    );
  },
);

export default Table;
