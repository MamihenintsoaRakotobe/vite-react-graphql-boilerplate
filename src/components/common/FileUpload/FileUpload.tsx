import { ChangeEventHandler, FC, MutableRefObject } from "react";
import AddIcon from '@mui/icons-material/Add';
import styles from './FileUpload.module.scss';
import DeleteIcon from '@mui/icons-material/Delete';
import { IconButton } from "@mui/material";
import DescriptionIcon from '@mui/icons-material/Description';
import { downloadBlob } from "../../../helpers/utils";

interface IProps {
    handleChange?:ChangeEventHandler<HTMLInputElement>;
    name:string;
    fileName?:string | null;
    isMultiple?:boolean;
    idAction:string;
    handleDeleteFile?:() => void;
    accept?:string;
    technicalSheetUploadRef?:MutableRefObject<any>
    file?:File | null
}

const FileUpload:FC<IProps> = ({
  handleChange,
  name = false,
  fileName,
  isMultiple=false,
  idAction,
  handleDeleteFile,
  accept,
  technicalSheetUploadRef,
  file,
}) => {
  
  

  return (
    <div className={styles.container}>
      <label htmlFor={idAction}>
        <input
          className={styles.input}
          id={idAction}
          name={idAction}
          type="file"
          onChange={handleChange}
          multiple={isMultiple}
          hidden
          accept={accept}
          ref={technicalSheetUploadRef}
        />

        <div className={styles.buttonContainer}>
          <AddIcon />
          {name}
        </div>
      </label>
      {fileName && (
        <div className={styles.filenameContainer}>
          <DescriptionIcon />
          <a className={styles.link} onClick={() =>file && downloadBlob(file,fileName)}>
            {fileName}
          </a>
          <IconButton onClick={handleDeleteFile}>
            <DeleteIcon color="error" fontSize="small"/>
          </IconButton>
        </div>
      )}
    </div>
      
  );
};

export default FileUpload;