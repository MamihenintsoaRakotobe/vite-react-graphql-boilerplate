import React, {
  CSSProperties,
  useMemo,
  FC,
  Dispatch,
  SetStateAction,
} from 'react';
import { DropEvent, FileRejection, useDropzone } from 'react-dropzone';
import styles from './Dropzone.module.scss';
import ClearIcon from '@mui/icons-material/Clear';
import { IconButton } from '@mui/material';
import FileUpload from './FileUpload';
import { useDispatch } from 'react-redux';
import { displayErrorNotification } from '@ducks/Snackbar.slice';
import { useTranslation } from 'react-i18next';

interface IProps  {
	maxSizeAllowed: number;
	buttonName:string;
	idAction:string;
	files: File[];
	setChoosenFile:Dispatch<SetStateAction<File | null>>;
	choosenFile:File | null;
	onDrop?:<T extends File>(acceptedFiles: T[], fileRejections: FileRejection[], event: DropEvent) => void;
	deleteImage: (file:File) => void;
}

const Dropzone: FC<IProps> = ({
  maxSizeAllowed,
  buttonName,
  idAction,
  files,
  setChoosenFile,
  choosenFile,
  onDrop,
  deleteImage,
}) => {

  const dispatch = useDispatch();
  const {t} = useTranslation();

  const sizeValidator = (file: File): any => {
    if (file.size > maxSizeAllowed) {
      dispatch(displayErrorNotification("maximum error"));
    }
    return null;
  };

  const {
    getRootProps,
    getInputProps,
    isDragAccept,
    isDragActive,
    isDragReject,
  } = useDropzone({
    validator: sizeValidator,
    accept: {
      'image/*': ['.jpeg', '.png'],
    } ,
    noClick:true,
    onDrop:onDrop,
  });



  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragAccept, isDragActive, isDragReject]
  );


  return (
    <div>
      <div className={styles.container}>
        <div {...getRootProps({ style })}>
          <input {...getInputProps({
            multiple: true,
          })} name={idAction} id={idAction}/>
          {files.length ===0 && (
            <p className={styles.text}>
              {t('article.additionalInformation.dragNdrop')}
            </p>
          )}
          {files.length > 0 && (

            <IconButton
              className={styles.deleteImageButton}
              onClick={() => deleteImage(choosenFile ?? files[files.length -1])}
            >
              <ClearIcon />
            </IconButton>
          )}

          {files.length > 0 && (
            <img
              className={styles.topImage}
              src={URL.createObjectURL(choosenFile ?? files[files.length -1])}
              alt={files[0].name.split('.')[0]}
            />
          )}
        </div>

      </div>
      <div className={styles.imageContainer}>
        {files.map((file, index) => (
          <div className={styles.imageWrapper} key={index} onClick={()=>setChoosenFile(file)}>
            <img
              className={styles.image}
              key={index}
              src={URL.createObjectURL(file)}
              alt={file.name.split('.')[0]}
            />
          </div>
        ))}
      </div>
      <div className={styles.buttonContainer}>
        <FileUpload
          name={buttonName}
          isMultiple={true}
          idAction={idAction}
          accept={"image/png,image/jpg,image/jpeg"}
        />
      </div>

    </div>
  );
};

export default Dropzone;

const baseStyle: CSSProperties = {
  width:"100%",
  height:"100%",
  position :'absolute',
  top: 0,
  right: 0,
  zIndex:5,
  display:'flex',
  flexDirection:'column',
  justifyContent:'center',
};


const activeStyle = {
  borderColor: '#2196f3',
};

const acceptStyle = {
  borderColor: '#00e676',
};

const rejectStyle = {
  borderColor: '#ff1744',
};
