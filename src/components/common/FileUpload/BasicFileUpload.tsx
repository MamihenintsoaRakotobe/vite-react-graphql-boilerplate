import React from 'react';
import { Button } from '../form';

interface IProps {
  triggerFileSelection: () => void
  fileRef: React.LegacyRef<HTMLInputElement>
  handleChange: React.ChangeEventHandler<HTMLInputElement>
  className?: string
  accept?: string
}

const BasicFileUpload: React.FC<IProps> = ({
  triggerFileSelection,
  fileRef,
  handleChange,
  className,
  accept = '.xlsx,.xls,csv,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf',
}) => {
  return (
    <Button
      className={className}
      color={'secondary'}
      onClick={triggerFileSelection}
      type="button"
    >
      Charger
      <input
        type="file"
        ref={fileRef}
        onChange={handleChange}
        accept={accept}
        hidden
      />
    </Button>
  );
};

export default BasicFileUpload;
