export const noFilterSelect = (t: (key: string) => string, value: string | number | null = "") => ({
  value,
  content: t("common.noFilter"),
});
