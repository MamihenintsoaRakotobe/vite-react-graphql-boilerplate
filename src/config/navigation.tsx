import { HomeScreen } from "@components/screen/home";
import { InProgress } from "@components/screen/inProgress";


interface RouteConnectedArray {
  path: RoutesConnected;
  element: JSX.Element;
}

export enum RoutesConnected {
  INDEX = "/",
  WIP = "/wip",
  
}

export const routeConnected: RouteConnectedArray[] = [
  {
    path: RoutesConnected.INDEX,
    element: <HomeScreen />,
  },
  
  {
    path: RoutesConnected.WIP,
    element: <InProgress />,
  },
];
