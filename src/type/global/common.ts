import { ReactNode } from "react";

export interface BasicObject {
  [key: string]: any;
}

export interface TabsContent {
  label: string;
  id: number;
  content: ReactNode;
}
