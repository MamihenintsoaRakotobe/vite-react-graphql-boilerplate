import { RoutesConnected } from "@config/navigation";

export interface BreadcrumbsArray {
  href: RoutesConnected;
  title: string;
}
