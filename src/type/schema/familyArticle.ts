export interface FamilyArticle {
  id: number;
  type: string;
  category: string;
  designation: string;
  metal: string;
  count: number;
}

interface ListType {
  type: string;
}

interface listCatergories {
  category: string;
}

export interface FilterFamilyArticle {
  listTypes: ListType[];
  listCatergories: listCatergories[];
}
