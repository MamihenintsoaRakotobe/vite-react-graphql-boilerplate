export enum ProviderOrderBuyType {
  BUY_SELL = 'Achat/Vente',
  STOCK = 'Stock',
}

export enum ProviderOrderTariffType {
  EMPTY = ' Tarif creux',
}

export enum ProviderOrderDocumentType {
  EMAIL = 'Email',
  CUTTING_BOOK = 'Carnet de coupe',
}
