export interface listingTabArticle {
  id: number;
  designation: string;
  type: string;
  family: string;
  strategy: string;
  supply: string;
  technicalSheet: string;
  reference: string;
  count: number;
}

export interface DataFamilyFilterArticle {
  id: number;
  designation: string;
}

export interface DataGetListFilterArticle {
  family: DataFamilyFilterArticle[];
  strategy: string[];
  supply: string[];
  technicalSheet: string[];
  category: string[];
}
