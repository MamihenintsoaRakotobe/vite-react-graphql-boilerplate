export const enum ContactJob {
  BUYER = "buyer",
  SITE_MANAGER = "siteManager",
  WORKS_MANAGER = "worksManager",
  BUSINESS_MANAGER = "businessManager",
  SHOPPING_ASSISTANT = "shoppingAssistant",
}
