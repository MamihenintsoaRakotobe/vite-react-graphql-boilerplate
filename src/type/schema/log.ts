interface UserFilterLog {
  name: string;
  id: number;
}

export interface GetListFilterLog {
  users: UserFilterLog[];
}
