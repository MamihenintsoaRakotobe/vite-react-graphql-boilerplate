# stage 1: BUILDER
FROM node:16-alpine AS builder
WORKDIR /app
COPY ./package.json ./
COPY ./package-lock.json ./
RUN npm ci
COPY . .
RUN npm run build

# stage 2: RUNNER
FROM node:16-alpine AS runner

# install simple http server for serving static content
# using express now to handle client-side routing
#RUN npm install -g http-server

# make the 'app' folder the current working directory
WORKDIR /app

ENV NODE_ENV production
COPY --from=builder /app/. .

EXPOSE 3000
CMD [ "node", "production-server/server.js" ]
