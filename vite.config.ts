import react from "@vitejs/plugin-react";
import { defineConfig } from "vite";
import { resolve } from "path";

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    outDir: 'production-server/dist',
  },
  plugins: [react()],
  resolve: {
    alias: [
      { find: "@gql", replacement: resolve(__dirname, "src/gql") },
      { find: "@components", replacement: resolve(__dirname, "src/components")},
      { find: "@ducks", replacement: resolve(__dirname, "src/ducks") },
      { find: "@helpers", replacement: resolve(__dirname, "src/helpers") },
     
      { find: "@store", replacement: resolve(__dirname, "src/store") },
      { find: "@styles", replacement: resolve(__dirname, "src/styles") },
      { find: "@translate", replacement: resolve(__dirname, "src/translate")},
      { find: "@validations", replacement: resolve(__dirname, "src/validations")},
      { find: "@hooks", replacement: resolve(__dirname, "src/hooks") },
      { find: "@graphql", replacement: resolve(__dirname, "src/graphql") },
      { find: "@config", replacement: resolve(__dirname, "src/config") },
      { find: "@type", replacement: resolve(__dirname, "src/type") },
    ],
  },
});
